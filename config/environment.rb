RACK_ENV = ENV['RACK_ENV'] ||= ENV['RACK_ENV'] ||= 'development' unless defined?(RACK_ENV)

require 'bundler/setup' # require bundler
Bundler.require # load gems, using bundler

require_all 'src'
require_relative 'database'
