DATABASE_URL = ENV['DATABASE_URL'] ||= ENV['DATABASE_URL'] ||=
                                         case RACK_ENV
                                         when 'development'
                                           'postgres://guarabot:guarabot@localhost/guarabot_development'
                                         when 'test'
                                           test_db_host = ENV['DB_HOST'] || 'localhost'
                                           "postgres://guarabot:guarabot@#{test_db_host}/guarabot_test"
                                         end

DB = Sequel.connect(DATABASE_URL)
