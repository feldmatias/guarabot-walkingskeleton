require_relative '../spec_helper'

describe Subject do
  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:name) }
  end

  describe 'valid' do
    it 'should be valid if name length is 5' do
      subject = described_class.new(name: 'fisica')
      expect(subject).to be_valid
    end

    it 'should be invalid valid if name length is 1' do
      subject = described_class.new(name: 'f')
      expect(subject).not_to be_valid
      expect(subject.errors).to have_key(:name)
    end
  end
end
