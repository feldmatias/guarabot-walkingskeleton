require_relative '../spec_helper'

describe SubjectsRepository do
  let(:repository) { described_class.new }

  describe 'get first subject' do
    it 'returns correct subject' do
      subject = Subject.new(name: 'fisica')
      repository.save(subject)
      result = repository.first
      expect(result.id).to eq(subject.id)
    end
  end
end
