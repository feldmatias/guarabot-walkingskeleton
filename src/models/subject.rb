class Subject
  include ActiveModel::Validations

  attr_accessor :id
  attr_reader :name

  validates_length_of :name, minimum: 2

  def initialize(params = {})
    @id = params[:id]
    @name = params[:name]
  end
end
