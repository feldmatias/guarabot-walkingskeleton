class SubjectsRepository < BaseRepository
  self.table_name = :subjects
  self.model_class = 'Subject'
  self.timestamps = false

  protected

  def changeset(subject)
    {
      name: subject.name
    }
  end
end
