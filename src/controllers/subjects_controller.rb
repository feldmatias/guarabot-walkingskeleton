class SubjectsController
  def get
    insert('fisica')
    insert('matematica')
    json_parse(SubjectsRepository.new.all)
  end

  def insert(name)
    subject = Subject.new(name: name)
    SubjectsRepository.new.save(subject)
  end

  def json_parse(subjects_list)
    # TODO: other object
    subjects = []
    subjects_list.each { |subject| subjects << { id: subject.id, name: subject.name } }
    subjects.to_json
  end
end
