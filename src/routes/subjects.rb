module Routing
  module Subjects
    def self.registered(app)
      app.get '/' do
        SubjectsController.new.get
      end

      app.get '/subjects' do
        SubjectsController.new.get
      end

      app.get '/error' do
        raise MYERROR
      end
    end
  end
end
