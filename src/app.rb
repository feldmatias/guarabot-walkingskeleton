require_relative '../config/environment'
require_all 'src/routes/*'
require_relative '../src/models/errors'
require_relative 'api_middleware'

class GuaraBot < Sinatra::Base
  use ApiMiddleware
  set :root, File.dirname(__FILE__)
  set show_exceptions: false

  error GuaraBotError do |error|
    status 400
    { error: 'guarabot_error: ' + error.class.to_s }.to_json
  end

  enable :sessions

  register Routing::Subjects
end
