class ApiMiddleware
  def initialize(app)
    @app = app
  end

  def call(env)
    puts(env['token'])
    puts('---------------------------------------')
    puts(env)
    if env['HTTP_TOKEN'] == 'MY_SECRET_TOKEN'
      @app.call(env)
    else
      Rack::Response.new({ error: 'error' }.to_json, 401, {}).finish
    end
  end
end
