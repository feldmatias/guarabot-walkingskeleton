# GuaraBot

### Heroku
- https://test-guarabot.herokuapp.com/

### Run
- rackup
- localhost:9292

### Tests
- rake

- migrations are rollbacked and migrated every time

### Database setup

Follow these steps to initialize the PostgreSQL databases:

1. Install PostgreSQL if needed. On Ubuntu you can do this by running:
`sudo apt-get install -y postgresql-9.5 postgresql-contrib postgresql-server-dev-9.5`
1. Create development and test databases by running:
`sudo -u postgres psql --dbname=postgres -f ./create_dev_and_test_dbs.sql`

### Sequel
- rake sequel:generate[name]   # Generate a new migration file `sequel:generate[create_books]`
- RACK_ENV=development rake sequel:migrate[version] # Migrate the database (you can specify the version with `db:migrate[N]`)
- RACK_ENV=development rake sequel:rollback[step]   # Rollback the database N steps (you can specify the version with `db:rollback[N]`)
- RACK_ENV=development rake sequel:remigrate        # Undo all migrations and migrate again
