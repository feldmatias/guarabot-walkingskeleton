CREATE ROLE guarabot WITH LOGIN PASSWORD 'guarabot'  CREATEDB;

CREATE DATABASE guarabot_development;
CREATE DATABASE guarabot_test;

GRANT ALL PRIVILEGES ON DATABASE "guarabot_development" to guarabot;
GRANT ALL PRIVILEGES ON DATABASE "guarabot_test" to guarabot;
